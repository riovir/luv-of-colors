import { resolve } from 'node:path';
import { defineConfig } from 'vite';

export default defineConfig({
	base: '/lab-of-colors/',
	publicDir: 'ui/public',
	build: {
		emptyOutDir: true,
		outDir: 'public',
		rollupOptions: {
			input: {
				'main': resolve(import.meta.dirname, 'index.html'),
				'example': resolve(import.meta.dirname, 'example.html'),
				'index': resolve(import.meta.dirname, 'src/index.ts'),
				'components': resolve(import.meta.dirname, 'ui/components.ts'),
				'components.define': resolve(import.meta.dirname, 'ui/components.define.ts'),
			},
			output: {
				entryFileNames: ({ name }) => {
					return ['index', 'main'].includes(name) ? 'assets/[name]-[hash].js' : '[name].js';
				},
			},
		},
	},
});
