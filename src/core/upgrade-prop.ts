export function upgradeProp<Host, Prop extends keyof Host>(host: Host, prop: Prop) {
	if (!Object.prototype.hasOwnProperty.call(host, prop)) { return; }
	const value = host[prop];
	delete host[prop];
	host[prop] = value;
}
