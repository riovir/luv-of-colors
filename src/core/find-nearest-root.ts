export function findNearestRoot(node: Node): Document | ShadowRoot | null {
	let currentNode: Node | null = node;
	while (currentNode) {
		if (currentNode instanceof Document || currentNode instanceof ShadowRoot) {
			return currentNode;
		}
		currentNode = currentNode.parentNode;
	}
	return null;
}
