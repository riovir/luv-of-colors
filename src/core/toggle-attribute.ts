export function toggleAttribute(host: Element, attribute: string, present = !host.hasAttribute(attribute)) {
	if (present) {
		host.setAttribute(attribute, '');
	}
	else {
		host.removeAttribute(attribute);
	}
}
