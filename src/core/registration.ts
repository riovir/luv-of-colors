export type Registrar = {
	addChildNode: (element: EventTarget) => void;
	removeChildNode: (element: EventTarget) => void;
};

