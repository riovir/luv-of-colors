export function css([part]: TemplateStringsArray): CSSStyleSheet {
	const sheet = new CSSStyleSheet();
	sheet.replaceSync(part ?? '');
	return sheet;
}
