import { Rgb, type Oklch, oklchToRgb, oklchToHex } from './colors/index.js';
import { css, findNearestRoot } from './core/index.js';
import { Registrar, upgradeProp } from './core/index.js';
import { isColorPicker, type ColorPicker } from './oklch-color-picker.js';

const sheet = css`
	:host {
		box-sizing: border-box;
		display: block;
		font-family: sans-serif;
	}
	.config-container {
		margin-bottom: var(--lab-spacer-3x, 12px);
	}

	input[type="color"] {
		width: var(--lab-spacer-16x, 64px);
		height: var(--lab-spacer-8x, 32px);
	}

	.palette-generator-range-controls {
		display: inline-grid;
		grid-template-columns: auto 1fr;
		gap: var(--lab-spacer-2x, 8px);
	}

	.palette-generator-range-controls hr {
		grid-column: 1 / -1;
		visibility: hidden;
	}
`;

const template = document.createElement('template');
template.innerHTML = /* html */`
<div class="config-container">
	<input id="base-color" type="color" value="#ffffff">
	<label for="base-color">Base color</label>
</div>
<details>
	<summary>Advanced options</summary>

	<div class="palette-generator-range-controls">
		<label for="hue-from">From hue</label>
		<input id="hue-from" type="number" value="100">
		<label for="hue-to">To hue</label>
		<input id="hue-to" type="number" value="100">
		<label for="hue-rage-type">Hue range type</label>
		<select id="hue-rage-type">
			<option value="linear">Linear</option>
			<option value="curved">Curved</option>
		</select>
		<hr />
		<label for="chroma-from">From chroma</label>
		<input id="chroma-from" type="number" value="0.2" step="0.01" max="0.4">
		<label for="chroma-to">To chroma</label>
		<input id="chroma-to" type="number" value="0.2" step="0.01" max="0.4">
		<label for="chroma-rage-type">Chroma range type</label>
		<select id="chroma-rage-type">
			<option value="linear">Linear</option>
			<option value="curved">Curved</option>
		</select>
	</div>
</details>
<slot></slot>
`;

export class OklchPaletteGenerator extends HTMLElement implements Registrar {
	declare registeredNodes: ColorPicker[];
	declare _target: HTMLElement | string;
	declare _baseColorInput: HTMLInputElement;
	declare _hueFromInput: HTMLInputElement;
	declare _hueToInput: HTMLInputElement;
	declare _hueRageTypeSelect: HTMLSelectElement;
	declare _chromaFromInput: HTMLInputElement;
	declare _chromaToInput: HTMLInputElement;
	declare _chromaRageTypeSelect: HTMLSelectElement;

	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.adoptedStyleSheets = [sheet];
		shadowRoot.appendChild(template.content.cloneNode(true));

		this.registeredNodes = [];

		this._updateBaseColor = this._updateBaseColor.bind(this);
		this._updatePalette = this._updatePalette.bind(this);
		this._updateColors = this._updateColors.bind(this);
		this._onChildNodeRegister = this._onChildNodeRegister.bind(this);

		this._target = this;
		this._baseColorInput = shadowRoot.querySelector<HTMLInputElement>('#base-color')!;
		this._baseColorInput.addEventListener('input', this._updateBaseColor);
		this._hueFromInput = shadowRoot.querySelector<HTMLInputElement>('#hue-from')!;
		this._hueFromInput.addEventListener('input', this._updatePalette);
		this._hueToInput = shadowRoot.querySelector<HTMLInputElement>('#hue-to')!;
		this._hueToInput.addEventListener('input', this._updatePalette);
		this._hueRageTypeSelect = shadowRoot.querySelector<HTMLSelectElement>('#hue-rage-type')!;
		this._hueRageTypeSelect.addEventListener('input', this._updatePalette);
		this._chromaFromInput = shadowRoot.querySelector<HTMLInputElement>('#chroma-from')!;
		this._chromaFromInput.addEventListener('input', this._updatePalette);
		this._chromaToInput = shadowRoot.querySelector<HTMLInputElement>('#chroma-to')!;
		this._chromaToInput.addEventListener('input', this._updatePalette);
		this._chromaRageTypeSelect = shadowRoot.querySelector<HTMLSelectElement>('#chroma-rage-type')!;
		this._chromaRageTypeSelect.addEventListener('input', this._updatePalette);

		this.addEventListener('child-node-register', this._onChildNodeRegister);
		this.addEventListener('hue-changed', this._updateColors);
		this.addEventListener('chroma-changed', this._updateColors);
		this.addEventListener('lightness-changed', this._updateColors);
	}

	connectedCallback() {
		upgradeProp(this, 'baseColorHex');
		upgradeProp(this, 'baseColorOklch');
		upgradeProp(this, 'colorNameBase');
		upgradeProp(this, 'target');

		if (this.hasAttribute('base-color-hex')) {
			this.baseColorHex = this.getAttribute('base-color-hex')!;
			Promise.resolve().then(this._updateBaseColor);
		}

		const target = this.getAttribute('target');
		if (target) {
			this.target = target;
		}
	}

	_updateBaseColor() {
		this._baseColorInput.style.color = `oklch(from ${this._baseColorInput.value} l c h)`;
		const [/* lightness */, chroma, hue] = this.baseColorOklch;
		this._hueFromInput.value = String(hue);
		this._hueToInput.value = String(hue);
		this._chromaFromInput.value = String(chroma);
		this._chromaToInput.value = String(chroma);
		this._updatePalette();
		this.dispatchEvent(new Event('base-color-changed', { composed: true, bubbles: true }));
	}

	_updatePalette() {
		this._generatePaletteOklch().forEach(([lightness, chroma, hue], index) => {
			Object.assign(this.registeredNodes[index]!, { hue, chroma, lightness });
		});
		this._updateColors();
	}

	_updateColors() {
		const { style } = this.targetElement;
		Object.entries(this.paletteOklch).forEach(([cssVariable, [lightness, chroma, hue]]) => {
			style.setProperty(cssVariable, `oklch(${lightness}% ${chroma} ${hue})`);
		});
	}

	_onChildNodeRegister({ target }: Event) {
		this.addChildNode(target!);
	}

	addChildNode(element: EventTarget) {
		if (!isColorPicker(element)) { return; }
		element._registrar = this;
		if (this.registeredNodes.includes(element)) { return; }
		this._updateChildNode(element);
		this.registeredNodes = [...this.registeredNodes, element];
		this._updateColors();
	}

	_updateChildNodes() {
		this.registeredNodes.forEach(node => this._updateChildNode(node));
	}

	_updateChildNode(picker: ColorPicker) {
		if (this.registeredNodes.includes(picker)) { return; }
		picker.textContent = this._getCssVariableOf(picker).replace('--', '');
	}

	removeChildNode(picker: EventTarget) {
		if (!(this.registeredNodes as unknown[]).includes(picker)) { return; }
		this.registeredNodes = this.registeredNodes.filter(el => el !== picker as unknown);
	}

	get target() {
		return this._target;
	}
	set target(value: HTMLElement | string) {
		if (!value) { return; }
		if (typeof value !== 'string' && !(value instanceof HTMLElement)) { return; }
		this._target = value;
	}

	get targetElement(): HTMLElement {
		if (this._target instanceof HTMLElement) { return this._target; }
		if (typeof this._target !== 'string') { return this; }

		const element = findNearestRoot(this)
				?.querySelector(this._target);
		return element instanceof HTMLElement ? element : this;
	}

	get colorNameBase() {
		return this.getAttribute('color-name-base') || 'color-custom';
	}
	set colorNameBase(value: string) {
		this.setAttribute('color-name-base', value);
		this._updateChildNodes();
	}

	get paletteOklch(): Record<string, Oklch> {
		return Object.fromEntries(this.registeredNodes.map(picker => [
			this._getCssVariableOf(picker), [picker.lightness, picker.chroma, picker.hue]
		]));
	}

	_generatePaletteOklch() {
		if (!this.registeredNodes.length) { return []; }
		const { lightness: lightnessFrom = 97 } = this.registeredNodes.at(0) ?? {};
		const { lightness: lightnessTo = 7 } = this.registeredNodes.at(-1) ?? {};
		const { hueFrom, hueTo, hueRangeType, chromaFrom, chromaTo, chromaRangeType } = this;

		const hueDiff = hueTo - hueFrom;
		const hueMultiplierOf = hueRangeType === 'curved' ?
			toMultiplierCurved({ from: lightnessFrom, to: lightnessTo }) :
			toMultiplierLinear({ from: lightnessFrom, to: lightnessTo });

		const chromaDiff = chromaTo - chromaFrom;
		const chromaMultiplierOf = chromaRangeType === 'curved' ?
			toMultiplierCurved({ from: lightnessFrom, to: lightnessTo }) :
			toMultiplierLinear({ from: lightnessFrom, to: lightnessTo });

		return this.registeredNodes
				.map(({ lightness }) => [
					lightness,
					chromaFrom + chromaMultiplierOf(lightness) * chromaDiff,
					hueFrom + hueMultiplierOf(lightness) * hueDiff,
				]);
	}

	get paletteRgb(): Record<string, Rgb> {
		return Object.fromEntries(Object.entries(this.paletteOklch)
				.map(([cssVariable, oklch]) => [cssVariable, oklchToRgb(oklch)]));
	}

	get baseColorHex() {
		return this._baseColorInput.value;
	}
	set baseColorHex(value: string) {
		this._baseColorInput.value = value;
		this._updateBaseColor();
	}

	get baseColorOklch() {
		const { color } = getComputedStyle(this._baseColorInput);
		const [lightness = 0, chroma = 0, hue = 0] = color.split(/[a-zA-Z() ]/).filter(Boolean).map(Number);
		return [lightness * 100, chroma, hue];
	}
	set baseColorOklch([lightness, chroma, hue]: [number, number, number]) {
		this.baseColorHex = oklchToHex([lightness, chroma, hue]);
	}

	get hueFrom() {
		return Number(this._hueFromInput.value);
	}
	get hueTo() {
		return Number(this._hueToInput.value);
	}
	get hueRangeType() {
		return this._hueRageTypeSelect.value;
	}

	get chromaFrom() {
		return Number(this._chromaFromInput.value);
	}
	get chromaTo() {
		return Number(this._chromaToInput.value);
	}
	get chromaRangeType() {
		return this._chromaRageTypeSelect.value;
	}

	_getNameOf({ name, lightness }: ColorPicker) {
		if (name) { return name; }
		const [first] = this.registeredNodes;
		if (!first) { return '10'; }
		return String(Math.abs(Math.round(10 + first.lightness - lightness)));
	}

	_getCssVariableOf(picker: ColorPicker) {
		return `--${this.colorNameBase}-${this._getNameOf(picker)}`;
	}
}

function toMultiplierLinear({ from, to }: { from: number; to: number }) {
	const diff = to - from;
	return (value: number) => diff === 0 ? 1 : (value - from) / diff;
}

function toMultiplierCurved({ from, to }: { from: number; to: number }) {
	const toMultiplier = toMultiplierLinear({ from, to });
	return (value: number) => {
		const result = toMultiplier(value);
		return (result * result);
	};
}
