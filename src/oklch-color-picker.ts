import { isHex, Oklch, oklchToHex, isOklch } from './colors/index.js';
import { css, toggleAttribute, type Registrar, upgradeProp } from './core/index.js';

const CHROMA_MAX = 0.4;

const sheet = css`
	:host {
		--_color-picker-preview-color = black;
		container-type: inline-size;
		box-sizing: border-box;
		min-width: 240px;
		font-family: sans-serif;
	}

	* {
		box-sizing: border-box;
	}

	:host([lightness-under-60]) {
		--_color-picker-preview-color: white;
	}

	.color-picker__container {
		display: grid;
		justify-items: stretch;
		align-items: center;
		gap: var(--lab-spacer-2x, 8px);
		width: 100%;
		height: 100%;
		min-height: var(--lab-spacer-8x, 32px);
	}

	.color-picker__color-preview {
		display: grid;
		grid-template-columns: 1fr auto;
		justify-items: stretch;
		align-items: center;
		padding: var(--lab-spacer-1x, 4px) var(--lab-spacer-2x, 8px);
		border-radius: var(--lab-spacer-radius, 8px);
		min-width: 28ch;
		min-height: var(--lab-spacer-8x, 32px);
		width: 100%;
		color: var(--_color-picker-preview-color);
		background-color: var(--color-picker-preview-color, oklch(from lightgrey l c h));
	}

	.color-picker__control {
		display: grid;
		width: 100%;
	}

	.color-picker__control > * {
		min-width: 100px;
	}

	@container (min-width: 16em) {
		.color-picker__container {
			grid-template-columns: 1fr 1fr;
		}

		.color-picker__color-preview {
			grid-column: 1 / -1;
		}
	}

	@container (min-width: 32em) {
		.color-picker__container {
			grid-template-columns: max-content 1fr 1fr;
		}

		.color-picker__color-preview {
			grid-column: unset;
		}
	}
`;

const template = document.createElement('template');
template.innerHTML = /* html */`
	<div class="color-picker__container">
		<div id="color-preview" class="color-picker__color-preview">
			<slot><div></div></slot>
			<div id="color-value"></div>
		</div>
		<div class="color-picker__control">
			<label id="hue">Hue</label>
			<input id="hue-number" aria-labelledby="hue" type="number" value="0" />
			<input id="hue-range" aria-labelledby="hue" type="range" value="0" min="0" max="360" />
		</div>
		<div class="color-picker__control">
			<label id="chroma">Chroma</label>
			<input id="chroma-number" aria-labelledby="chroma" type="number" value="0.2" step="0.01" />
			<input id="chroma-range" aria-labelledby="chroma" type="range" value="0.2" min="0" max="${CHROMA_MAX}" step="0.01" />
		</div>
	</div>
`;

export type ColorPicker = Pick<OklchColorPicker, 'name' | 'hue' | 'chroma' | 'lightness' | '_registrar'> & Element;

/**
 * @element color-picker
 *
 * @prop {string} [colorNameBase=''] - base name of the CSS variable
 * @prop {string} [suffix=''] - an optional suffix added to the CSS variable
 *
 * @readonly {string} cssVariable - name of the CSS variable backing the color picker following the --[colorNameBase]-[suffix] formula
 *
 * @fires child-node-register - fired when when connected to the DOM
 * @fires lightness-changed - fired when when lightness has changed
 */
export class OklchColorPicker extends HTMLElement implements ColorPicker {
	declare _registrar: Registrar;
	declare _hueInputRange: HTMLInputElement;
	declare _hueInputNumber: HTMLInputElement;
	declare _chromaInputRange: HTMLInputElement;
	declare _chromaInputNumber: HTMLInputElement;
	declare _colorPreviewNode: HTMLElement;
	declare _colorValueNode: HTMLElement;
	declare _colorPreviewStyle: CSSStyleDeclaration;

	constructor() {
		super();
		const shadowRoot = this.attachShadow({ mode: 'open' });
		shadowRoot.adoptedStyleSheets = [sheet];
		shadowRoot.appendChild(template.content.cloneNode(true));

		this._updateHue = this._updateHue.bind(this);
		this._updateChroma = this._updateChroma.bind(this);

		this._colorPreviewNode = shadowRoot.querySelector('#color-preview')!;
		this._colorValueNode = shadowRoot.querySelector('#color-value')!;

		this._hueInputRange = shadowRoot.querySelector<HTMLInputElement>('#hue-range')!;
		this._hueInputRange.addEventListener('input', () => this._updateHue());
		this._hueInputNumber = shadowRoot.querySelector('#hue-number')!;
		this._hueInputNumber.addEventListener('input', ({ target }: Event) => {
			this._hueInputRange.value = (target as HTMLInputElement).value || '0';
			this._updateHue();
		});

		this._chromaInputRange = shadowRoot.querySelector<HTMLInputElement>('#chroma-range')!;
		this._chromaInputRange.addEventListener('input', () => this._updateChroma());
		this._chromaInputNumber = shadowRoot.querySelector<HTMLInputElement>('#chroma-number')!;
		this._chromaInputNumber.addEventListener('input', ({ target }: Event) => {
			this._chromaInputRange.value = (target as HTMLInputElement).value || '100';
			this._updateChroma();
		});
	}

	connectedCallback() {
		upgradeProp(this, 'name');
		upgradeProp(this, 'oklch');
		upgradeProp(this, 'hex');
		upgradeProp(this, 'lightness');
		upgradeProp(this, 'chroma');
		upgradeProp(this, 'hue');

		this._colorPreviewStyle = getComputedStyle(this._colorPreviewNode);
		if (this.hasAttribute('lightness')) {
			this.lightness = Number(this.getAttribute('lightness'));
		}
		if (this.hasAttribute('chroma')) {
			this.chroma = Number(this.getAttribute('chroma'));
		}
		if (this.hasAttribute('hue')) {
			this.hue = Number(this.getAttribute('hue'));
		}
		if (this.hasAttribute('hex')) {
			this.hex = this.getAttribute('hex')!;
		}
		this._updateColor();

		Promise.resolve().then(() => this.dispatchEvent(new Event('child-node-register', { bubbles: true })));
	}

	disconnectedCallback() {
		this._registrar?.removeChildNode?.(this);
	}

	get oklch() {
		if (!this._colorPreviewStyle) { return [0, 0, 0]; }
		const { backgroundColor } = this._colorPreviewStyle;
		const [lightness = 0, chroma = 0, hue = 0] = backgroundColor.split(/[a-zA-Z() ]/).filter(Boolean).map(Number);
		return [lightness * 100, chroma, hue];
	}
	set oklch([lightness, chroma, hue]: Oklch) {
		if (!isOklch([lightness, chroma, hue])) { return; }
		this.style.setProperty('--color-picker-preview-color', `oklch(${lightness}% ${chroma} ${hue}`);
		this._updateColor();
	}

	get hex(): string {
		return oklchToHex([this.lightness, this.chroma, this.hue]).toUpperCase();
	}
	set hex(value: string) {
		if (!isHex(value)) { return; }
		this.style.setProperty('--color-picker-preview-color', `oklch(from ${value} l c h)`);
		this._updateColor();
	}

	_updateColor() {
		const [/* lightness */, chroma, hue] = this.oklch;
		this._chromaInputRange.value = String(chroma);
		this._chromaInputNumber.value = String(chroma);
		this._hueInputRange.value = String(hue);
		this._hueInputNumber.value = String(hue);
		this._colorValueNode.textContent = this.hex;
		toggleAttribute(this, 'lightness-under-60', this.lightness < 60);
	}

	get lightness(): number {
		return this.oklch[0];
	}
	set lightness(value: number) {
		if (value < 0 || 100 < value || this.lightness === value) { return; }
		const [/* lightness */, chroma, hue] = this.oklch;
		this.oklch = [value, chroma, hue];
		this.dispatchEvent(new Event('lightness-changed', { bubbles: true }));
	}

	get chroma(): number {
		return this.oklch[1];
	}
	set chroma(value: number) {
		this._updateChroma(value);
	}

	_updateChroma(value: number = Number(this._chromaInputRange.value)) {
		if (value < 0 || this.chroma === value) { return; }
		const [lightness, /* chroma */, hue] = this.oklch;
		this.oklch = [lightness, value, hue];
		this.dispatchEvent(new Event('chroma-changed', { bubbles: true }));
	}

	get hue(): number {
		return this.oklch[2];
	}
	set hue(value: number) {
		this._updateHue(value);
	}

	_updateHue(value: number  = Number(this._hueInputRange.value)) {
		if (value < 0 || 360 < value || this.hue === value) { return; }
		const [lightness, chroma] = this.oklch;
		this.oklch = [lightness, chroma, value];
		this.dispatchEvent(new Event('hue-changed', { bubbles: true }));
	}

	get name(): string {
		return this.getAttribute('name') ?? '';
	}
	set name(value: string) {
		this.setAttribute('name', value);
	}
}

export function isColorPicker(value: unknown | ColorPicker): value is ColorPicker {
	if (!value || typeof value !== 'object') { return false; }
	const candidate = value as ColorPicker;
	return typeof candidate.hue === 'number' &&
		typeof candidate.chroma === 'number' &&
		typeof candidate.lightness === 'number' &&
		typeof candidate.textContent === 'string';
}
