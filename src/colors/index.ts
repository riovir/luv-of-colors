export type Rgb = [number, number, number];
export type Oklch = [number, number, number];

export function isHex(value: string) {
	return /^#(?:[0-9a-fA-F]{3,4}){1,2}$/.test(value);
}

export function oklchToRgb([lightness, chroma, hue]: Oklch) {
	return colorStringToRgba(`oklch(${lightness}% ${chroma} ${hue})`);
}

export function oklchToHex(oklch: Oklch): string {
	const [r, g, b] = oklchToRgb(oklch);
	const toHex = (value: number) => {
		const hex = value.toString(16);
		return hex.length == 1 ? '0' + hex : hex;
	};
	return `#${toHex(r)}${toHex(g)}${toHex(b)}`;
}

export function isOklch(value: unknown): value is Oklch {
	if (!Array.isArray(value)) { return false; }
	const [lightness, chroma, hue] = value;
	return 0 <= lightness && lightness <= 100 && 0 < chroma && 0 <= hue && hue <= 360;
}

function colorStringToRgba(string: string): Rgb {
	const canvas = document.createElement('canvas');
	Object.assign(canvas, { width: 1, height: 1 });
	const context = canvas.getContext('2d', { willReadFrequently: true });
	if (!context) { throw new Error('Unable to get 2D canvas context'); }
	context.fillStyle = string;
	context.fillRect(0, 0, 1, 1);
	const [r, g, b] = context.getImageData(0, 0, 1, 1).data;
	return [checkNumber(r), checkNumber(g), checkNumber(b)];
}

function checkNumber(value: unknown): number {
	if (typeof value !== 'number' || isNaN(value)) {
		throw new Error(`Value is not a number: ${value}`);
	}
	return value as number;
}
