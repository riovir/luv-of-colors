import { OklchColorPicker, OklchPaletteGenerator } from './components.js';

customElements.define('oklch-color-picker', OklchColorPicker);
customElements.define('oklch-palette-generator', OklchPaletteGenerator);
